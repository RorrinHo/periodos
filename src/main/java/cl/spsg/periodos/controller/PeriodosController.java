package cl.spsg.periodos.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cl.spsg.periodos.dto.PeriodosDTO;
import cl.spsg.periodos.service.PeriodosService;

@RestController
@RequestMapping("/periodos")

public class PeriodosController {

	@Autowired
	PeriodosService periodosService;

	Logger logger = LoggerFactory.getLogger(PeriodosController.class);

	@GetMapping("fechas")
	public ResponseEntity<PeriodosDTO> obtenerPeriodos() {

		logger.info("INICIO CONTROLLER: obtenerPeriodos: ");
		PeriodosDTO periodos = null;
		try {
			periodos = periodosService.obtenerPeriodos();
		} catch (Exception e) {
			return ResponseEntity.noContent().build();
		}

		return ResponseEntity.ok(periodos);

	}

}