package cl.spsg.periodos.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import cl.spsg.periodos.dto.PeriodosDTO;

@Service
public class PeriodosServiceImpl implements PeriodosService {

	@Value("${app.url.periodos}")
	private String urlPeriodos;

	@Autowired
	private RestTemplate restTemplate;

	Logger logger = LoggerFactory.getLogger(PeriodosServiceImpl.class);

	public PeriodosDTO obtenerPeriodos() {

		logger.info("INICIO PeriodosServiceImpl: obtenerPeriodos: ");

		PeriodosDTO response = restTemplate.getForObject(urlPeriodos, PeriodosDTO.class);

		List<LocalDate> periodosFaltantes = new ArrayList<LocalDate>();
		LocalDate fechaAux = response.getFechaCreacion();

		while (fechaAux.isBefore(response.getFechaFin().plusMonths(1))) {
			periodosFaltantes.add(fechaAux);
			fechaAux = fechaAux.plusMonths(1);
		}

		periodosFaltantes.removeAll(response.getFechas());
		response.setFechasFaltantes(periodosFaltantes);

		return response;

	}

}