package cl.spsg.periodos.service;

import cl.spsg.periodos.dto.PeriodosDTO;

public interface PeriodosService {

	public PeriodosDTO obtenerPeriodos();

}