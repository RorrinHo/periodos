# Compilar y ejecutar el proyecto

Realizado bajo:

-	Java 8
-	Spring boot 2.3.1
-	maven 3.6.3


Ingresar al directorio raiz del proyecto "periodos" mediante consola y ejecutar lo siguiente:

	mvn package

Esperar hasta que termine.


# Tener en cuenta

Se debe levantar el proyecto que genera las fechas aleatorias.
	

# Ejecutar

Para ejecutar el proyecto ingresar al directorio "periodos"  mediante consola y ejecutar lo siguiente:

	java -jar target/periodos-0.0.1-SNAPSHOT.jar  

# Visualizar

Para visualizar el resultado se debe ingresar a la siguiente url en el navegador o postman:

	http://localhost:8181/periodos/fechas

# Swagger
	
	http://localhost:8181/swagger-ui.html
	

	

	
